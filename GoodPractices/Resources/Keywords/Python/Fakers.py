from faker import Faker

class Fakers(object):

    def __init__(self):
        self.fake = Faker()
    
    def fake_address(self):
        return self.fake.address()
    
    def fake_name(self):
        return f"{self.fake.first_name()} {self.fake.last_name()}"
    