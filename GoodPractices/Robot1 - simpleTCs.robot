*** Settings ***
Library                 SeleniumLibrary
Library                 OperatingSystem
Library                 String
Library                 Collections

*** Variables ***
${URL1}                 http://wp.pl/
${URL2}                 https://onet.pl/
${URL3}                 http://wikipedia.pl/
${URL4}                 http://robotframework.com/
${URL5}                 https://interia.pl/

*** Test Cases ***
Validate Availability
   [Template]                   Open URL
   ${URL1}
   ${URL2}
   ${URL3}
   ${URL4}
   ${URL5}

*** Keywords ***
Open URL
   [Arguments]                  ${URL}
   Open Browser                 ${URL}     Chrome
