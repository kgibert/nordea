*** Settings ***
Library              Resources/Keywords/Python/Fakers.py

*** Test Cases ***
TC_001_Test_Fake
   ${fake_name}                          Fakers.Fake Name
   ${fake_address}                       Fake Address
   Log                                   ${fake_name}
   Log                                   ${fake_address}

