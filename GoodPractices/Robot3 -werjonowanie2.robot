*** Settings ***
Metadata             Release        2.2.101.54
...                  Pattern        2.2.101.[1-54]OR2.2.[1-100].*OR2.[1-1].*OR[1-1].*

*** Test Cases ***
TC_001
   [Tags]         1.1.200.201
   No Operation

TC_002
   [Tags]         2.2.200.201
   No Operation

TC_003
   [Tags]         2.3.200.201
   No Operation

TC_004
   [Tags]         2.2.1.299
   No Operation

TC_005
   [Tags]         10.1.1.1
   No Operation

TC_006
   [Tags]         1.2.200.201
   No Operation