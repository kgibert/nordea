from win32cryptcon import CRYPT_BLOB_VER3
class GenerateRunCommand(object):

    def build_versions(self, bld_ver, pch_ver, min_ver, maj_ver):
        cmd = f"{maj_ver}.{min_ver}.{pch_ver}.1"
        if bld_ver > 2:
            for i in range(2, bld_ver+1):
                cmd += f"OR{maj_ver}.{min_ver}.{pch_ver}.{i}"
        return cmd
    
    def patch_versions(self, pch_ver, min_ver, maj_ver):
        cmd = f"{maj_ver}.{min_ver}.1.*"
        if pch_ver > 2:
            for i in range(2, pch_ver):
                cmd += f"OR{maj_ver}.{min_ver}.{i}.*"
        return cmd
    
    def minor_versions(self, min_ver, maj_ver):
        cmd = f"{maj_ver}.1"
        if min_ver > 2:
            for i in range(2, min_ver):
                cmd += f"OR{maj_ver}.{i}.*"
        return cmd
    
    def major_versions(self, maj_ver):
        cmd = f"1.*"
        if maj_ver > 2:
            for i in range(2, maj_ver):
                cmd += f"OR{i}.*"
        return cmd

    def generate(self, ver):
        command = '';
        
        ver_list = ver.split(".")
        
        bld_ver = int(ver_list[3])
        pch_ver = int(ver_list[2])
        min_ver = int(ver_list[1])
        maj_ver = int(ver_list[0])
        
        build = self.build_versions(bld_ver, pch_ver, min_ver, maj_ver)
        patch = self.patch_versions(pch_ver, min_ver, maj_ver)
        minor = self.minor_versions(min_ver, maj_ver)
        major = self.major_versions(maj_ver)

        command = f"{build}OR{patch}OR{minor}OR{major}"
        print (command)
           
if __name__ == '__main__':
    version = "2.2.101.54"
    GenerateRunCommand().generate(version)
    
    
    