*** Settings ***
Library              DateTime
Test Teardown        Log                     TeardownTest
Suite Teardown       Log                     TeardownSuite
Suite Setup          Log                     SetupSuite
Library              Script/AssertCheck.py

*** Keywords ***
Log Keyword
   ${output}               Assert Pass
   Log                     Keyword
   Should Be Equal         PASS            ${output}

*** Test Cases ***
TC_001
   Log Keyword

TC_002
   No Operation

TC_003
   No Operation

TC_004
   No Operation

TC_005
   No Operation

TC_006
   No Operation

TC_007
   No Operation

TC_008
   No Operation

TC_009
   No Operation


