*** Settings ***
Library              String

*** Test Cases ***
ForTC old syntax
   @{list}          Split String              %{Path}                   ;
   :FOR             ${var}                    IN                        @{list}
   \                ${var_length}                    Get Length                ${var}
   \                Log                       ${var_length}
   \                Run Keyword If            'krzysztof' in '${var}'   Log                            ${var}

ForTC new syntax
   @{list}          Split String              %{Path}                   ;
   FOR              ${var}                    IN                        @{list}
      ${var_length}           Get Length                ${var}
      Log              ${var_length}
      Run Keyword If   'krzysztof' in '${var}'   Log                       ${var}
   END