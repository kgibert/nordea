*** Settings ***

*** Keywords ***
the account balance is ${value}
   No Operation

the card is valid
   No Operation

the machine contains enough money
   No Operation

the Account Holder requests ${value}
   Log                                     ${value}

the ATM should dispense ${value}
   No Operation

the account balance should be ${value}
   No Operation

the card should be returned
   No Operation

*** Test Cases ***
Account has sufficient funds
   Given the account balance is $100
   And the card is valid
   And the machine contains enough money
   When the Account Holder requests $20
   Then the ATM should dispense $20
   And the account balance should be $80
   And the card should be returned
