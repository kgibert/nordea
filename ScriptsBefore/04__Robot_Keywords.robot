*** Settings ***
Library                  AutoItLibrary                 10                                  True
Library                  String

*** Variables ***
${saveWindowTitle}       Save As

*** Keywords ***
Wait For Window Custom
   ${winTitle}                   Win Get Title                       ${saveWindowTitle}
   Run Keyword If                '${winTitle}'=='${Empty}'           Fail

Draw Line
   [Arguments]                   ${startX}=0                         ${startY}=0                           ${endX}=0                ${endY}=0
   Mouse Move                    ${startX}                           ${startY}
   Mouse Down                    Left
   Mouse Move                    ${endX}                             ${endY}
   Mouse Up                      Left

Close Popup
   Control Click                 [CLASS:#32770]                      Do&n't Save                           Button2

*** Test Cases ***
AutoITSample
   Run                           mspaint
   ${windowTitle}                Set Variable                        Untitled - Paint
   ${mainWindow}                 Wait For Active Window              ${windowTitle}
   Control Get Focus             ${windowTitle}
   Win Set State                 ${windowTitle}                      ${Empty}                              3
   Sleep                         2s

   # Draw Line
   Draw Line                     200                                 300                                   300                      400
   Sleep                         3s

   # Change color
   Mouse Click                   Left                                937                                   82
   Wait For Active Window        Edit Colors
   Control Set Text              Edit Colors                         ${Empty}                              Edit4                    64
   Control Set Text              Edit Colors                         ${Empty}                              Edit5                    222
   Control Set Text              Edit Colors                         ${Empty}                              Edit6                    128
   Control Click                 Edit Colors                         ${Empty}                              Button2


   # Draw Line again
   Draw Line                     300                                 400                                   400                      200
   Sleep                         3s

   # Save As
   Mouse Click                   Left                                26                                    42
   Mouse Click                   Left                                92                                    200
   ${saveDlgWindow}              Set Variable                        ${saveWindowTitle}
   Win Wait Not Active           ${mainWindow}
   Wait Until Keyword Succeeds   1 min                               2 sec                                 Wait For Window Custom
   ${winTitle}                   Win Get Title                       ${saveDlgWindow}
   ${editBtn}                    Set Variable If                     '${winTitle}'=='${saveWindowTitle}'   Edit1
   Sleep                         2s

   Run Keyword If                '${editBtn}'!='Edit1'               Run Keywords
   ...                           Log                                 zepsuło się                           AND
   ...                           Get Screen Image                    C:/Users/krzysztof.gibert/Documents/1.png   AND
   ...                           Fail

   # Set Name
   ${fileName}                   Generate Random String              10
   Control Focus                 ${winTitle}                         ${Empty}                              ${editBtn}
   Control Set Text              ${winTitle}                         ${Empty}                              ${editBtn}               ${fileName}.png
   Sleep                         3s

   # Save
   ${handle}                     Control Get Handle                  [CLASS:#32770]                        Save                     Button2
   Run Keyword If                '${handle}'=='0x0000000000000000'   Fail
   Control Click                 [CLASS:#32770]                      Save                                  Button2
   Win Close                     ${windowTitle}
   Close Popup





