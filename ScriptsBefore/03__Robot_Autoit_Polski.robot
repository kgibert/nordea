*** Settings ***
Library                   AutoItLibrary                  10                           True
Library                   String

*** Keywords ***
Wait For Window Custom
    ${winTitle}                    Win Get Title                Zapisz jako
    Run Keyword If                 '${winTitle}'=='${Empty}'    Fail

*** Test Cases ***
AutoITSample
    Run                            mspaint                      
    ${windowTitle}                 Set Variable                 Bez\xa0tytułu - Paint
    ${mainWindow}                  Wait For Active Window       ${windowTitle}
    Control Get Focus              ${windowTitle}
    Win Set State                  ${windowTitle}               ${Empty}                        3
    Sleep                          2s

    # Draw Line
    Mouse Move                     200                          300
    Mouse Down                     Left
    Mouse Move                     300                          400
    Mouse Up                       Left
    Sleep                          3s

    # Save As
    Mouse Click                    Left                         26                              42
    Mouse Click                    Left                         92                              200
    ${saveDlgWindow}               Set Variable                 Zapisz
    Win Wait Not Active            ${mainWindow}
    Wait Until Keyword Succeeds    1 min                        2 sec                           Wait For Window Custom
    ${winTitle}                    Win Get Title                ${saveDlgWindow}
    ${editBtn}                     Set Variable If              '${winTitle}'=='Zapisz jako'    Edit1
    Sleep                          2s

    # Set Name
    ${fileName}                    Generate Random String       10
    Control Focus                  ${winTitle}                  ${Empty}                        ${editBtn}
    Control Set Text               ${winTitle}                  ${Empty}                        ${editBtn}                ${fileName}.png
    Sleep                          3s

    # Save
    Control Click                  [CLASS:#32770]               Zapisz                          Button2
    ${text}=                       Control Get Text             Zapisz jako                     ${Empty}                  [CLASS:Edit; INSTANCE:1]
    ${y}=                          Control Get Pos Y            [CLASS:#32770]                  Cancel                    [CLASS:Button; INSTANCE:3]
    Log                            ${text}
    Log                            ${y}
    Win Close                      Bez\xa0tytułu - Paint
