*** Settings ***
Library              String

*** Variables ***
${message}           ${REPORT_FILE}

*** Test Cases ***
Robot1
   Set Test Message   Hello RF only when only when positive

   Log                ${message}

   ${length}          Get Length            ${message}
   Log Many           Length:               ${length}    .

   ${log_msg}         Format String         Length:{0}{end}      ${length}      end=.
   Log                ${log_msg}
   Log                Length:${length}.
   
   ${img}             Set Variable          <img src="C:/Users/krzysztof.gibert/Pictures/Saved Pictures/pobrane.jpg" />
   Log                ${img}
   Log                ${img}                html=true            console=true
   Log                <h2>BIG</h2>          html=true

   Run Keyword If     ${length}>56          Fail                 Failed
   ${match}           Should Match Regexp   ${message}           (report)
   ${new_report}      Replace String        ${match}[1]          po             ${Empty}