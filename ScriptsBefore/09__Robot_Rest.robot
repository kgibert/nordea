*** Settings ***
Variables                 Resources/API/Regres/local.py
Library                   REST                                ${HOST}
Resource                  Resources/API/Regres/api.resource
Suite Setup               Login - Get Token                   ${USER.email}      ${USER.password}

*** Variables ***
${USER.name}              Bob
${USER.job}               Tester
${USER.email}             eve.holt@reqres.in
${USER.password}          cityslicka

*** Test Cases ***
Create User - API/USERS
   Users - Create User                 ${USER.name}       ${USER.job}
   OUTPUT                              response

User Log Model
   Log                                 ${USER.name}
   Log                                 ${USER.surname}
   Log                                 ${USER.job}
   Log                                 ${USER.email}
   Log                                 ${USER.password}
   Log                                 ${USER.id}