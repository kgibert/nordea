*** Settings ***
Library              Resources/Robot_API/BuiltInCustom.py
Library              Resources/Robot_API/LoggerCustom.py
Resource             Resources/common.resource

*** Test Cases ***
TC_001_Fail
   BuiltInCustom.Fail                     This is custom message

TC_002_Fail
   Fail                                   This is custom message

TC_003_Fail
   Fail Ext                               This is custom message

TC_004_Fail
   BuiltIn.Fail                           This is custom message

TC_005_Fail
   Fail Custom                            This is custom message

TC_006_Log
   ${string}                              ${count}                 LoggerCustom.Remove String   message custom   cus
   Builtin.Log                            ${string}: ${count}