import os.path
from robot.libraries.BuiltIn import BuiltIn
from robot.libraries.Screenshot import Screenshot

class BuiltInCustom(BuiltIn):
    """Biblioteka rozszerzająca BuiltIn o nowe funkcjonalności

    Example
    """
    ROBOT_LIBRARY_VERSION = 1.1

    def __init__(self):
        pass
    
    def fail(self, argument):
        """Takes one argument *fail* with it and make screenshot

        Examples:
        | BuiltInCustom.Fail | message |
        | BuiltInCustom.Fail | message2 |
        """
        print('*HTML* New Fail')
        path = "C:/Users/krzysztof.gibert/tmp"
        Screenshot().set_screenshot_directory(path)
        Screenshot().take_screenshot()
        BuiltIn().fail(self, argument)
        
    def failExt(self, argument):
        """Takes one argument *fail* with it and make screenshot

        Examples:
        | Fail Ext | message |
        | Fail Ext | message2 |
        """
        print('*HTML* New Fail')
        path = "C:/Users/krzysztof.gibert/tmp"
        Screenshot().set_screenshot_directory(path)
        Screenshot().take_screenshot()
        BuiltIn().fail(self, argument)
    