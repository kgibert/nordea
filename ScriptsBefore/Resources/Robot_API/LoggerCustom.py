from robot.libraries.String import String
from robot.libraries.BuiltIn import BuiltIn

class LoggerCustom(String):

    ROBOT_LIBRARY_VERSION = 1.0

    def __init__(self):
        pass
    
    def remove_string(self, string, *words):
        new_string = String().remove_string(string, *words)
        count = BuiltIn().get_length(new_string)
        return new_string, count