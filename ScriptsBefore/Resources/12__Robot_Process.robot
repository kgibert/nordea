*** Settings ***
Library              Process
Suite Teardown       Terminate All Processes   kill=True

*** Test Cases ***
Cmd Line Test
   Run Process               python               Resources/Commands/GitCommands.py   2s                 alias=p1
   Log                       after p1
   Run Process               python               Resources/Commands/GitCommands.py   2s                 alias=p2
   Log                       after p2

   ${result} =               Get Process Result   p2
   Log                       ${result.rc}
   Log                       ${result.stdout}
   Log                       ${result.stderr}

   Start Process             python               Resources/Commands/GitCommands.py   git                alias=p3
   Log                       after p3
   ${proc}=                  Wait For Process     p3                                  timeout=120 secs   on_timeout=kill
   Log                       ${proc.stdout}

   ${inline}                 Run Process          git                                 --version
   Log                       ${inline.stdout}

