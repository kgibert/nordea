import requests
from xml.etree import ElementTree

class StubSoap(object):

    ROBOT_LIBRARY_VERSION = 1.0

    def __init__(self):
        pass

    def call_stub_soap(self):
        url="http://www.dneonline.com/calculator.asmx?wsdl"
        headers = {'content-type': 'application/soap+xml'}
        body = """<?xml version="1.0" encoding="utf-8"?>
                <soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">
                  <soap12:Body>
                    <Subtract xmlns="http://tempuri.org/">
                      <intA>10</intA>
                      <intB>15</intB>
                    </Subtract>
                  </soap12:Body>
                </soap12:Envelope>"""

        response = requests.post(url,data=body,headers=headers)
        print (response.content)
        
        tree = ElementTree.fromstring(response.content)
        value = tree.find(".//{http://tempuri.org/}SubtractResult")
        print(value.text)
        