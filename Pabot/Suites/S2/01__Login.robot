*** Settings ***
Metadata             REQ                               https://tracker.nordea.com/jira/browse/RF-1320
Documentation        As a Administrator I want to Login to Application
Resource             ../../Resources/base.resource
Test Setup           Test Suite Setup Login
Test Teardown        Test Suite Teardown Login

*** Test Cases ***
As a Administrator I want to Login to Application with correct credentials
   Given Application is running
   When I click SignIn button
   And I fill login: "admin" and password: "admin123!"
   And I click Login button
   Then Login Form not visible
   And Im in application dashboard

As a Administrator I want to Login to Application with incorrect credentials
   Given Application is running
   When I click SignIn button
   And I fill login: "admin21" and password: "admin123!123"
   And I click Login button
   Then Login Form visible

As a Administrator I want to Login to Application with incorrect password
   Given Application is running
   When I click SignIn button
   And I fill login: "admin" and password: "admin123!12"
   And I click Login button
   Then Login Form visible

As a Administrator I want to Login to Application with incorrect username
   Given Application is running
   When I click SignIn button
   And I fill login: "admin123" and password: "admin123!"
   And I click Login button
   Then Login Form visible

As a Administrator I want to Login to Application with empty credentials
   Given Application is running
   When I click SignIn button
   And I fill login: "${Empty}" and password: "${Empty}"
   And I click Login button
   Then Login Form visible
