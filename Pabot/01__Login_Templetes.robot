*** Settings ***
Metadata                  REQ                            https://tracker.nordea.com/jira/browse/RF-1320
Documentation             As a Administrator I want to Login to Application
Resource                  Resources/base.resource
Library                   Finds                          ${SUT_NAME}
Test Template             And I fill login: "${username}" and password: "${password}"
Test Setup                Suite Templete Setup
Test Teardown             Suite Template Teardown

*** Keywords ***
Suite Templete Setup
   Test Suite Setup Login
   Given Application is running
   When I click SignIn button

Suite Template Teardown
   And I click Login button
   # dodatkowa praca na generyczne kroki testowe
   Test Suite Teardown Login

*** Test Cases ***        ${username}                    ${password}
correct credentials       admin                          admin123!
incorrect credentials     admin21                        admin123!123
incorrect password        admin                          admin123!12
incorrect username        admin123                       admin123!
empty credentials         ${Empty}                       ${Empty}
