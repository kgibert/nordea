*** Settings ***
Library              OperatingSystem
Library              pabot.pabotlib

*** Test Cases ***
Loop1
   FOR               ${element}               IN RANGE                 30
      Sleep             1
      Log               1:${element}
      Append To File    file_with_variable.txt   ${\n}1:${element}${\n}
   END