*** Settings ***
Library              OperatingSystem
Library              pabot.pabotlib
Suite Setup          Run Setup Only Once   Create File              file_with_variable.txt

*** Test Cases ***
Loop2
   FOR                   ${element}               IN RANGE                 30
      Sleep                 1
      Log                   2:${element}
      Append To File        file_with_variable.txt   ${\n}2:${element}${\n}
   END