from pywinauto import *
from robot.libraries.BuiltIn import BuiltIn
from Finds import Finds 
from robot.utils.asserts import *

class UsersFind(object):

    ROBOT_LIBRARY_VERSION = 1.0

    def __init__(self, arg=None): 
        self.title = arg
        self.finds = Finds(self.title)
        
    def find_in_table(self, username, name, role, status, action):
        BuiltIn().import_resource('../Resources/Pages/UsersPage/UsersLocs.resource')
        
        usernames = BuiltIn().get_variable_value('${USERS_tableUsername_py_col}')
        columns = BuiltIn().get_variable_value('${USERS_tableColumns_py_col}')

        element_usernames = self.finds.find_element(usernames)
        
        # remove buttons
        usernames_filtered = []
        for i in range(len(element_usernames)):
            element = element_usernames[i].children_texts()[0]
            if element != 'Edit' and element != 'Block' and element != 'Delete':
                usernames_filtered.append(element)
            
        print(usernames_filtered)
        
        #search for correct row
        idx = usernames_filtered.index(username)
        columns_count = 5
            
        user_row = []
        for i in range(idx*columns_count, (idx*columns_count)+columns_count):
            element = self.finds.find_element(columns+f'[{i}]'+'.children_texts()')
            user_row.append(element[0]) if element else None
            
        print(user_row)
        
        assert_equal(username, user_row[0])
        assert_equal(name, user_row[1])
        assert_equal(role, user_row[2])
        assert_equal(status, user_row[3])
            