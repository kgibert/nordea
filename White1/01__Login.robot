*** Settings ***
Metadata             REQ                                https://tracker.nordea.com/jira/browse/RF-1320
Documentation        As a Administrator I want to Login to Application
Resource             Resources/base.resource

*** Test Cases ***
As a Administrator I want to Login to Application with correct credentials
   Given Application is running
   When I click SignIn button
   And I fill login: admin and password: admin123!
   And I click Login button
   Then Im in application dashboard

As a Administrator I want to Login to Application with incorrect credentials
   No Operation

As a Administrator I want to Login to Application with incorrect password
   No Operation

As a Administrator I want to Login to Application with incorrect username
   No Operation

As a Administrator I want to Login to Application with empty credentials
   No Operation