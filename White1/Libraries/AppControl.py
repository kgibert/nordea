import psutil
from WhiteLibrary.keywords.application import ApplicationKeywords

class AppControl(object):

    ROBOT_LIBRARY_VERSION = 1.0

    def __init__(self):
        self.state = self._State()
        self.app = ApplicationKeywords(self.state)

    def kill_application(self, name):
        pid = self.find_procs_by_name(name)
        if pid is not None:
            self.app.attach_application_by_id(pid)
            self.app.close_application();
     
    def find_procs_by_name(self, name): 
        for proc in psutil.process_iter():
            if proc.name() == name: 
                return proc.pid
            
    class _State(object):
        def __init__(self):
            self.app = None