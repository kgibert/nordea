from WhiteLibrary.utils.wait import Wait
from TestStack.White import Application, WhiteException
from AppControl import AppControl

class Waits(object):

    ROBOT_LIBRARY_VERSION = 1.0

    def __init__(self):
        pass

    def wait_for_application(self, name):

        def _search_window():          
            pid = AppControl().find_procs_by_name(name)
            if pid is None:
                return False
            
            hack = {"sut": None}
            try:
                hack["sut"] = Application.Attach(pid)
                return True
            except WhiteException:
                return False
            
        exception_message = "Custom Wait"
        Wait.until_true(lambda: _search_window(), 10, exception_message)