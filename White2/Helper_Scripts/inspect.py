from pywinauto import Application
app = Application(backend='uia').connect(title_re='Mr Buggy 7.*')
dlg = app.window(title_re='Mr Buggy 7.*')
dlg.print_control_identifiers(filename='inspect.txt')
