*** Settings ***
Library                   AutoItLibrary                 10                                  True
Library                   String

*** Variables ***
${saveWindowTitle}        Save As
${windowTitle}            Untitled - Paint
${maximizeFlag}           3
${saveWindowClass}        [CLASS:#32770]
${filenameInput}          Edit1

*** Keywords ***
Wait For Window Custom
   ${winTitle}                   Win Get Title                       ${saveWindowTitle}
   Run Keyword If                '${winTitle}'=='${Empty}'           Fail

Draw Line
   [Arguments]                   @{}                                 ${startX}=0                           ${startY}=0               ${endX}=0                 ${endY}=0
   Mouse Move                    ${startX}                           ${startY}
   Mouse Down                    Left
   Mouse Move                    ${endX}                             ${endY}
   Mouse Up                      Left

Close Popup
   Control Click                 ${saveWindowClass}                  Do&n't Save                           Button2

Run Application MSpaint
   Run                           mspaint
   ${mainWindow}                 Wait For Active Window              ${windowTitle}
   Control Get Focus             ${windowTitle}
   Win Set State                 ${windowTitle}                      ${Empty}                              ${maximizeFlag}
   [Return]                      ${mainWindow}

Change Color
   [Arguments]                   ${r}=0                              ${g}=0                                ${b}=0
   Mouse Click                   Left                                937                                   82
   Wait For Active Window        Edit Colors
   Control Set Text              Edit Colors                         ${Empty}                              Edit4                     ${r}
   Control Set Text              Edit Colors                         ${Empty}                              Edit5                     ${g}
   Control Set Text              Edit Colors                         ${Empty}                              Edit6                     ${b}
   Control Click                 Edit Colors                         ${Empty}                              Button2

Save As
   [Arguments]                   ${mainWindow}
   Mouse Click                   Left                                26                                    42
   Mouse Click                   Left                                92                                    200
   ${saveDlgWindow}              Set Variable                        ${saveWindowTitle}
   Win Wait Not Active           ${mainWindow}
   Wait Until Keyword Succeeds   1 min                               2 sec                                 Wait For Window Custom
   ${winTitle}                   Win Get Title                       ${saveDlgWindow}
   ${editBtn}                    Set Variable If                     '${winTitle}'=='${saveWindowTitle}'   ${filenameInput}
   Run Keyword If                '${editBtn}'!='${filenameInput}'    Run Keywords
   ...                           Log                                 zepsuło się                           AND
   ...                           Get Screen Image                    C:/Users/krzysztof.gibert/Documents/1.png   AND
   ...                           Fail
   [Return]                      ${editBtn}

Set Filename Input
   [Arguments]                   ${editBtn}
   ${fileName}                   Generate Random String              10
   Control Focus                 ${saveWindowTitle}                  ${Empty}                              ${editBtn}
   Control Set Text              ${saveWindowTitle}                  ${Empty}                              ${editBtn}                ${fileName}.png

Click Save
   ${handle}                     Control Get Handle                  [CLASS:#32770]                        Save                      Button2
   Run Keyword If                '${handle}'=='0x0000000000000000'   Fail
   Control Click                 [CLASS:#32770]                      Save                                  Button2

Kill Application
   Win Close                     ${windowTitle}
   Close Popup

#new
Regular Expression My Name: ${name:\w+} and birthday ${birthday:\d{2\}-\d{2\}-\d{4\}}
   Log                           My Name: ${name} and birthday: ${birthday}

Regular Expression Ver: ${ver:\d+\.\d+\.\d+build\d+}
   Log                           Ver: ${ver}

Draw Line Template Test
   [Arguments]                   ${startX}=0                         ${startY}=0                           ${endX}=0                 ${endY}=0
   Run Application MSpaint
   FOR                           ${item}                             IN RANGE                              5                         45                        10
      ${itemInt}                    Convert To Integer                  ${item}
      Draw Line                     startX=${itemInt+${startX}}         startY=${itemInt+${startY}}           endX=${itemInt+${endX}}   endY=${itemInt+${endY}}
   END
   Kill Application


*** Test Cases ***
Regular Expression TC
   Regular Expression My Name: Krzysztof and birthday 10-10-2019

Draw Line Template?
   [Tags]                        Ignore
   [Template]                    Draw Line
   startX=200                    startY=200                          endX=300                              endY=200
   startX=200                    startY=200                          endX=300                              endY=200

Draw Line Loop Templete
   [Template]                    Draw Line Template Test
   200                           300                                 400                                   300
   600                           200                                 200                                   500
   300                           500                                 400                                   300

AutoITSample
   [Tags]                        Smoke
   ${mainWindow}                 Run Application MSpaint
   Change Color                  r=150                               g=5                                   b=111
   # Draw Rectangle
   Draw Line                     startX=200                          startY=200                            endX=300                  endY=200
   Draw Line                     startX=300                          startY=200                            endX=300                  endY=300
   Draw Line                     startX=300                          startY=300                            endX=200                  endY=300
   Draw Line                     startX=200                          startY=300                            endX=200                  endY=200
   ${editBtn}                    Save As                             ${mainWindow}
   Set Filename Input            ${editBtn}
   Click Save
   Kill Application

Regular Expression Ver
   [Template]                    Regular Expression Ver: ${ver}
   11100build123
   11.18.12330build4555
   11.18.xbuild4555
