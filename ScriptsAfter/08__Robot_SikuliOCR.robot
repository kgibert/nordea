*** Settings ***
Library              SikuliLibrary
Library              String

*** Test Cases ***
SikuliOCR
   Open Application            notepad
   Type With Modifiers         \ue000                       WIN
   # Press Special Key         DOWN\ue002
   Add Image Path              Images
   Set Ocr Text Read           true
   Wait Until Screen Contain   notepad2.png=0.58            10
   Highlight                   notepad2.png=0.58            10
   Input Text                  notepad2.png=0.58            Lorem Ipsum Robot Framework 0.97 similarity
   ${text}                     Get Text
   Log                         ${text}
   Close Application           Untiled - Notepad






   # Open Application          "notepad" ${REPORT_FILE}
   # Type With Modifiers       \ue000                       WIN
   # Set Ocr Text Read         true
   # ${text}=                  Get Text
   # Log                       ${text}
   # ${match}=                 Get Regexp Matches           ${text}   maintain line




   # Open Application          "mspaint" Images\\book.png
   # Add Image Path            Images
   # Set Ocr Text Read         true
   # Type With Modifiers       \ue000                       WIN
   # # check screen
   # # ${cordinates}=          Get Screen Coordinates
   # # Highlight Region        ${cordinates}                5
   # Click                     zoom.png
   # ${text}=                  Get Text
   # Log                       ${text}
