from robot.utils import DotDict

def get_variables(env='DEV'):    
    if env == 'DEV':
        VARS = {"ENV_NAME": "Development",
               "TIMEOUT": "10",
               "CL": "2"}
    elif env == 'INT':
        VARS = {"ENV_NAME": "Integration",
               "TIMEOUT": "5",
               "CL": "4"}
    elif env == 'PROD':
        VARS = {"ENV_NAME": "Production",
               "TIMEOUT": "3",
               "CL": "5"}
    else:
        raise Exception("Environment not specified!")

    return {
        "VARS": DotDict(VARS)
    }