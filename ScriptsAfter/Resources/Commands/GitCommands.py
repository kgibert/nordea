import time, subprocess, sys, re
from builtins import range
from robot.libraries.Process import Process

class GitCommands(object):

    def output(self):
        for i in range(5):
            time.sleep(2)
            print(f"{i}")
    
    def check_git_version(self):
        print (f"Git version call")
        command = "git --version"
        result = subprocess.run(command, stdout=subprocess.PIPE)
        parsed_result = re.findall("\d.\d+.\d+.windows.\d+", result.stdout.decode('utf-8'))
        print(f'Result: {parsed_result[0]}') 
           
if __name__ == '__main__':
    arg = sys.argv[1]
    
    if  arg == '2s':
        GitCommands().output()
        
    if arg == 'git': 
        GitCommands().check_git_version()
    
    