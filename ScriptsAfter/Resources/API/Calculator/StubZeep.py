from zeep import Client

class StubZeep(object):

    ROBOT_LIBRARY_VERSION = 1.0

    def __init__(self):
        pass

    def zeep_stub_subtract(self):
        url="http://www.dneonline.com/calculator.asmx?wsdl"
        client = Client(wsdl=url)
        result = client.service.Subtract(10, 15)
        assert result == -5
        print (result)
