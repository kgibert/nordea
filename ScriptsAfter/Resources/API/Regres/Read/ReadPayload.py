import json;

class ReadPayload(object):

    ROBOT_LIBRARY_VERSION = 1.0

    def __init__(self):
        pass

    def get_token_payload(self, email, password):
        payload = {
                     "email": email,
                     "password": password
                  }
        return json.dumps(payload)
