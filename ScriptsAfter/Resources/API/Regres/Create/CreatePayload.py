import json;

class CreatePayload(object):

    ROBOT_LIBRARY_VERSION = 1.0

    def __init__(self):
        pass

    def create_user_payload(self, name, job):
        payload = {
                     "name": name,
                     "job": job
                  }
        return json.dumps(payload)
