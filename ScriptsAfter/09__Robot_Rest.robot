*** Settings ***
Variables                 Resources/API/Regres/local.py
Library                   REST                                ${HOST}            False
Resource                  Resources/API/Regres/api.resource
Suite Setup               Login - Get Token                   ${USER.email}      ${USER.password}

*** Variables ***
#${USER.name}             BobScalar
&{USER}                   name=Bob
${USER.job}               Tester
${USER.email}             eve.holt@reqres.in
${USER.password}          cityslicka


*** Test Cases ***
Create User - API/USERS
   ${USER.id}=                         Users - Create     ${USER.name}       ${USER.job}
   OUTPUT                              response
   #${USER.name}                       Set Variable       Nowy Bob

User Log Model
   Log                                 ${USER.name}
   Log                                 ${USER}[name]
   Log                                 ${USER.job}
   Log                                 ${USER.email}
   Log                                 ${USER.password}
   Log                                 ${USER.id}

Get User - API/USERS
   ${USER.id}                          Set Variable       2
   Users - Get User Name               ${USER.id}

