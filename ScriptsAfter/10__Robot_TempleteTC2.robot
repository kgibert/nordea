*** Settings ***
Variables              Resources/API/Regres/local.py
Documentation          HOST:                               ${HOST}
Metadata               HOST:                               ${HOST}
Library                REST                                ${HOST}
Resource               Resources/API/Regres/api.resource
Suite Setup            Login - Get Token                   ${USER.email}   ${USER.password}
Test Template          Get User - API/USERS

*** Variables ***
${USER.email}          eve.holt@reqres.in
${USER.password}       cityslicka

*** Test Cases ***     id
User id 1              1
User id 2              2
User id 3              3

*** Keywords ***
Get User - API/USERS
   [Arguments]                         ${id}
   Users - Get User Name               ${id}
