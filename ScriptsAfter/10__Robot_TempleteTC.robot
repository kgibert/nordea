*** Settings ***
Variables                 Resources/API/Regres/local.py
Documentation             HOST:                               ${HOST}
Metadata                  HOST:                               ${HOST}
Library                   REST                                ${HOST}
Resource                  Resources/API/Regres/api.resource
Suite Setup               Login - Get Token                   ${USER.email}   ${USER.password}
Test Template             Create User - API/USERS

*** Variables ***
${USER.email}             eve.holt@reqres.in
${USER.password}          cityslicka

*** Test Cases ***        Name                                Job
Valid User                Bob                                 Tester
Same Name User            Bob                                 Coder
Empty User                ${Empty}                            ${Empty}

*** Keywords ***
Create User - API/USERS
   [Arguments]                         ${name}         ${job}
   Users - Create                      ${name}         ${job}
