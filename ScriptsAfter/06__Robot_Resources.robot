*** Settings ***
Suite Setup               Log                               ${VARS.ENV_NAME}
Metadata                  Username                          ${USER}
Metadata                  Confidence Level                  ${VARS.CL}
Library                   AutoItLibrary                     10                               True
Library                   String
Variables                 Resources/Variables/common.py
Resource                  ${RES}/MSPaint/mspaint.resource
Resource                  ${RES}/common.resource
Resource                  Resources/MSPaint/Templetes/drawline.resource

*** Test Cases ***
Regular Expression TC
   Regular Expression My Name: Krzysztof and birthday 10-10-2019

Draw Line Template?
   [Tags]                            Ignore
   [Template]                        Draw Line
   startX=200                        startY=200                       endX=300        endY=200
   startX=200                        startY=200                       endX=300        endY=200

Draw Line Loop Templete
   [Template]                        Draw Line Template Test
   200                               300                              400             300
   600                               200                              200             500
   300                               500                              400             300

AutoITSample
   [Tags]                            Smoke
   ${mainWindow}                     Run Application MSpaint
   Change Color                      r=150                            g=5             b=111
   # Draw Rectangle
   Draw Line                         startX=200                       startY=200      endX=300   endY=200
   Draw Line                         startX=300                       startY=200      endX=300   endY=300
   Draw Line                         startX=300                       startY=300      endX=200   endY=300
   Draw Line                         startX=200                       startY=300      endX=200   endY=200
   ${editBtn}                        Save As                          ${mainWindow}
   Set Filename Input                ${editBtn}
   Click Save
   Kill Application

Regular Expression Ver
   [Template]                        Regular Expression Ver: ${ver}
   11100build123
   11.18.12330build4555
   11.18.xbuild4555
