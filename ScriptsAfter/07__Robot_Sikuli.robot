*** Settings ***
Library              SikuliLibrary

*** Test Cases ***
Sikuli
   [Tags]                                 Smoke
   #Set Show Actions                      True
   Open Application                       mspaint
   Type With Modifiers                    \ue000                      WIN
   Add Image Path                         Images

   # Similarity
   Set Min Similarity                     0.2
   Wait Until Screen Contain              mspaint_star.png=0.37       10

   # Nie można
   #${element}=                           Wait Until Screen Contain   mspaint_star.png=0.37   10
   #Click                                 ${element}

   # Mozna wczytac koordynaty, nie trzeba szukac za kazdym razem
   #${element}=                           Get Image Coordinates       mspaint_star.png=0.37
   #Click Region                          ${element}

   # Dopasowanie
   Click Nth                              mspaint_star.png=0.37       4                       0.37

   # Coordinates
   ${cordinates}=                         Get Image Coordinates       mspaint_star.png
   ${x}=                                  Evaluate                    ${cordinates}[0]/2
   ${x}=                                  Convert To Integer          ${x}
   ${y}=                                  Evaluate                    ${cordinates}[1]/2
   ${y}=                                  Convert To Integer          ${y}
   Click                                  mspaint_star.png            ${x}                    ${y}

   # Klik zbyt wysokie dopasowanie
   Click                                  mspaint_yellow.png=0.99

   #Klik z Najlepszym dopasowaniem
   Click                                  mspaint_3d.png

   # Wait similarity nie wpływa na klik
   Wait Until Screen Contain              3d_spray.png=0.71           20
   Click                                  3d_spray.png
   Click                                  3d_redcolor.png=0.9         32                      9

   # Draw
   Click                                  3d_white.png
   Mouse Down                             LEFT
   Click                                  3d_white.png                30                      30
