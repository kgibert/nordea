*** Settings ***
Library                   AutoItLibrary                 10                                  True
Library                   String

*** Variables ***
${saveWindowTitle}        Save As
${windowTitle}            Untitled - Paint
${maximizeFlag}           3
${saveWindowClass}        [CLASS:#32770]
${filenameInput}          Edit1

*** Keywords ***
Wait For Window Custom
   ${winTitle}                   Win Get Title                       ${saveWindowTitle}
   Run Keyword If                '${winTitle}'=='${Empty}'           Fail

Draw Line
   [Arguments]                   @{}                                 ${startX}=0                           ${startY}=0              ${endX}=0         ${endY}=0
   Mouse Move                    ${startX}                           ${startY}
   Mouse Down                    Left
   Mouse Move                    ${endX}                             ${endY}
   Mouse Up                      Left

Close Popup
   Control Click                 ${saveWindowClass}                  Do&n't Save                           Button2

Run Application MSpaint
   Run                           mspaint
   ${mainWindow}                 Wait For Active Window              ${windowTitle}
   Control Get Focus             ${windowTitle}
   Win Set State                 ${windowTitle}                      ${Empty}                              ${maximizeFlag}
   [Return]                      ${mainWindow}

Change Color
   [Arguments]                   ${r}=0                              ${g}=0                                ${b}=0
   Mouse Click                   Left                                937                                   82
   Wait For Active Window        Edit Colors
   Control Set Text              Edit Colors                         ${Empty}                              Edit4                    ${r}
   Control Set Text              Edit Colors                         ${Empty}                              Edit5                    ${g}
   Control Set Text              Edit Colors                         ${Empty}                              Edit6                    ${b}
   Control Click                 Edit Colors                         ${Empty}                              Button2

Save Picture
   [Arguments]                   ${mainWindow}
   Mouse Click                   Left                                26                                    42
   Mouse Click                   Left                                92                                    200
   ${saveDlgWindow}              Set Variable                        ${saveWindowTitle}
   Win Wait Not Active           ${mainWindow}
   Wait Until Keyword Succeeds   1 min                               2 sec                                 Wait For Window Custom
   ${winTitle}                   Win Get Title                       ${saveDlgWindow}
   ${editBtn}                    Set Variable If                     '${winTitle}'=='${saveWindowTitle}'   ${filenameInput}
   Run Keyword If                '${editBtn}'!='${filenameInput}'    Run Keywords
   ...                           Log                                 zepsuło się                           AND
   ...                           Get Screen Image                    C:/Users/krzysztof.gibert/Documents/1.png   AND
   ...                           Fail
   [Return]                      ${editBtn}

Set Filename Input
   [Arguments]                   ${editBtn}
   ${fileName}                   Generate Random String              10
   Control Focus                 ${saveWindowTitle}                  ${Empty}                              ${editBtn}
   Control Set Text              ${saveWindowTitle}                  ${Empty}                              ${editBtn}               ${fileName}.png

Click Save
   ${handle}                     Control Get Handle                  [CLASS:#32770]                        Save                     Button2
   Run Keyword If                '${handle}'=='0x0000000000000000'   Fail
   Control Click                 [CLASS:#32770]                      Save                                  Button2

Kill Application
   Win Close                     ${windowTitle}
   Close Popup

*** Test Cases ***
AutoITSample
   ${mainWindow}                 Run Application MSpaint
   Change Color                  r=64                                g=111                                 b=111
   # Draw Rectangle
   Draw Line                     startX=200                          startY=200                            endX=300                 endY=200
   Draw Line                     startX=300                          startY=200                            endX=300                 endY=300
   Draw Line                     startX=300                          startY=300                            endX=200                 endY=300
   Draw Line                     startX=200                          startY=300                            endX=200                 endY=200
   ${editBtn}                    Save Picture                        ${mainWindow}
   Set Filename Input            ${editBtn}
   Click Save
   Kill Application
