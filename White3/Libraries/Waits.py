from WhiteLibrary.utils.wait import Wait
from TestStack.White import Application, WhiteException
from AppControl import AppControl
from Finds import Finds
from pywinauto import *

class Waits(object):

    ROBOT_LIBRARY_VERSION = 1.0

    def __init__(self, arg=None):
        self.title = arg
        self.app = None

    def wait_for_application(self, name):

        def _search_window():          
            pid = AppControl().find_procs_by_name(name)
            if pid is None:
                return False
            
            hack = {"sut": None}
            try:
                hack["sut"] = Application.Attach(pid)
                return True
            except WhiteException:
                return False
            
        exception_message = "Custom Wait"
        Wait.until_true(lambda: _search_window(), 10, exception_message)
        
    def wait_not_visible_py(self, name):
        self.app = Finds(self.title)
        element = self.app.find_element(name)
        element.wait_not('visible')
        
    def wait_visible_py(self, name):
        self.app = Finds(self.title)
        try:
            timings.wait_until_passes(10, .5, lambda: self.wait_find_element(name), True)
        except Exception as e:
            print("timed out")
            print(e)
            
    def wait_find_element(self, name):
        try:
            element = self.app.find_element(name)
            timings.wait_until_passes(10, .5, lambda: element.is_visible(), True)
            return True
        except Exception as e:
            return False

    def wait_input_contains_text(self, name, text):
        self.app = Finds(self.title)
        element = self.app.find_element(name)
        timings.wait_until(10, .5, lambda: self.wait_text(text, element))  
        print (f'Text: {element.get_value()} , Text Expected:{text}') 
 
    def wait_text(self, text, element): 
        if element.get_value()==text:
            return True
        return False
        
    def wait_not_ready_py(self, name):
        self.app = Finds(self.title)
        try:
            timings.wait_until_passes(3, .5, lambda: self.wait_find_ready_element(name))
            raise WhiteException("element found")
        except WhiteException as e:
            raise e
        except Exception as e:
            exception_status = True
            print(e)
            print("element not found")
                 
    def wait_find_ready_element(self, name):
        try:
            element = self.app.find_element(name)
            return True
        except Exception as e:
            raise Exception(e)
            return False