from Models.State import State
from AppControl import AppControl
from WhiteLibrary.keywords.window import WindowKeywords
from TestStack.White.UIItems import *
from TestStack.White.UIItems.Finders import SearchCriteria
from System.Windows.Automation import ControlType

class Finders(object):

    ROBOT_LIBRARY_VERSION = 1.0
    ROBOT_LIBRARY_SCOPE = 'GLOBAL'

    def __init__(self, args=None):
        self.state = State()
        self.process = args

    def find_label_by_name(self, control_name, window_name): 
        self.state.app = AppControl().attach_application(self.process)
        WindowKeywords(self.state).attach_window(window_name)
        element = self.state.window.Get[Label](control_name)
        return element

    def find_button_by_text(self, id, window_name):
        self.state.app = AppControl().attach_application(self.process)
        WindowKeywords(self.state).attach_window(window_name)
        element = self.state.window.Get[Button](SearchCriteria.ByText(id))
        print(element)
        
    def find_edit_by_text(self, id, window_name):
        self.state.app = AppControl().attach_application(self.process)
        WindowKeywords(self.state).attach_window(window_name)
        element = self.state.window.Get(SearchCriteria.ByText(id).AndControlType(ControlType.Edit))
        print(element)
        
    def find_edits(self, window_name):
        self.state.app = AppControl().attach_application(self.process)
        WindowKeywords(self.state).attach_window(window_name)
        element = self.state.window.GetMultiple(SearchCriteria.ByControlType(ControlType.Edit))
        print(element)