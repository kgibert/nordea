import psutil
from WhiteLibrary.keywords.application import ApplicationKeywords
from Models.State import State

class AppControl(object):

    ROBOT_LIBRARY_VERSION = 1.0

    def __init__(self):
        self.state = State()
        self.app = ApplicationKeywords(self.state)

    def kill_application(self, name):
        pid = self.find_procs_by_name(name)
        if pid is not None:
            self.app.attach_application_by_id(pid)
            self.app.close_application();
     
    @staticmethod
    def find_procs_by_name(name): 
        for proc in psutil.process_iter():
            if proc.name() == name: 
                return proc.pid
            
    def get_application_status(self, name):
        pid = self.find_procs_by_name(name)
        if pid is None:
            return False
        return True
    
    def attach_application(self, name):  
        pid = self.find_procs_by_name(name)
        if pid is not None:
            self.app.attach_application_by_id(pid)
            return self.state.app