from pywinauto import *

class Finds(object):

    ROBOT_LIBRARY_VERSION = 1.0
    ROBOT_LIBRARY_SCOPE = 'GLOBAL'

    def __init__(self):
        pass

    def __init__(self, arg=None):
        self.title = arg
        self.dlg = None
        self.app = None
        
    def init_app(self):
        self.app = Application(backend='uia').connect(title_re=f'{self.title}.*')
        self.dlg = self.app.window(title_re=f'{self.title}.*')
        
    def find_element(self, name): 
        self.init_app()
        element = eval("self.dlg."+name)
        return element
        
    def fill_input(self, name, text):
        element = self.find_element(name)
        element.set_text(text)
        
    def click_element(self, name):
        element = self.find_element(name)
        try:
            element.click()
        except:
            element.invoke()
        
    def get_texts(self, name):
        element = self.find_element(name)
        print (element.texts())
            
    def get_controls_tree(self):
        self.init_app()
        self.app.dlg.print_control_identifiers()     
        
    def select_combobox(self, name, value):
        element = self.find_element(name)   
        element.select(value)   
        element.set_focus()
        
