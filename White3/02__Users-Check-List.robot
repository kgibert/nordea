*** Settings ***
Metadata                                  REQ                                        https://tracker.nordea.com/jira/browse/RF-220
Documentation                             v\As a Administrator I want to check users list
Resource                                  Resources/base.resource
Resource                                  Resources/Pages/UsersPage/UsersKwds.resource
Resource                                  Resources/Common/CommonKwds.resource
Library                                   Libraries/Finds.py                         ${SUT_NAME}

*** Test Cases ***
TC_001_Check_Admin_Present
   [Tags]                                     Smoke
   Go to Users Module
   #Finds.Get Controls Tree
   #Finds.Get Texts                           ${USERS_main_py_tbl}
   UsersKwds.Find in the List                 admin                  First Admin   Admin   Active   ${Empty}

TC_002_Add_Admin_And_Check_List_Changed
   Go to Users Module
   #just to show how to do it with combobox
   Finds.Select Combobox                      ComboBox2              Blocked
   Sleep                                      5

